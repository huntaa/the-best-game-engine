DEBUG := 1
project_name = Game
libraries = glfw3

ifeq ($(DEBUG), 1)
	CFLAGS = -O0 -g --std=c++17 -Iinclude -openmp -pthread -lGLEW -lGL -lSOIL
	mode = debug
else
	CFLAGS = -O3 --std=c++17 -Iinclude -openmp -pthread -lGLEW -lGL -lSOIL
	mode = release
endif

source_files = $(wildcard src/*.cpp)
objs = $(patsubst src/%.cpp, .objs/%.o, $(source_files))
dependancies = $(patsubst src/%.cpp,.deps/%.d,$(source_files))

libs = $(shell pkg-config --libs $(libraries))

output_dir = build/$(mode)

CC=clang++

$(shell mkdir -p .deps 2>/dev/null)
$(shell mkdir -p .objs 2>/dev/null)

$(shell mkdir -p $(output_dir) 2>/dev/null)

.PHONY:all
all:$(output_dir)/$(project_name)


.objs/$(notdir %.o): src/%.cpp
	$(CC) $(CFLAGS)  -MF $(patsubst %.cpp,.deps/%.d,$(notdir $<)) -MMD -c -o $@ $<


-include $(dependancies)

$(output_dir)/$(project_name): $(objs)
	$(CC) $(CFLAGS) $(LINKFLAGS)  $^ -o $@ $(libs)
	
.PHONY:run
run:all
	$(output_dir)/$(project_name)

.PHONY:clean
clean:
	rm -rf .deps .objs build >/dev/null 2>&1
