#include "task-queue.hpp"
#include "components.hpp"

TaskQueueWrapper::TaskQueueWrapper(int queue_len) {
    task_queue = new TaskQueue(queue_len);
    sem_init(&tasks_full, 0, queue_len);
    sem_init(&tasks_empty, 0, 0);

};

void TaskQueueWrapper::push(Task task) {
    sem_wait(&tasks_full);
    task_queue->push(task);
    sem_post(&tasks_empty);
}

bool TaskQueueWrapper::pop(Task& ret) {
    sem_wait(&tasks_empty);
    bool result = task_queue->pop(ret);
    sem_post(&tasks_full);
    return result;
}


TaskQueueWrapper::~TaskQueueWrapper() {
    delete task_queue;
}

void free_workers(Context *context) {
    int num_workers = context->num_workers;
    int i = 0;

    // for (i = 0; i < num_workers; ++i) {
    //     // queue_put(context->todo, NULL);
    //     context->push(NULL);
    // }

    for (i = 0; i < num_workers; ++i) {
        if (pthread_join(context->threads[i], NULL) != 0) {
            perror("pthread_join");
            exit(1);
        }
    }

    //queue_free(context->todo);
    delete context->todo;
    delete[] (context->threads);
    delete context;
}


void* worker_thread(void* arg) {
    Context* context = (Context*) arg;
    Task task;
    while(true) {
        context->todo->pop(task);
        printf("system: %d  value: %d\n", task.st, task.number);
    }
    return NULL;
}

Context::Context(int num_workers, int queue_size, ComponentTable* component_table) {
    todo = new TaskQueueWrapper(queue_size);
    num_workers = num_workers;
    threads = new pthread_t[num_workers];
    component_table = component_table;
}

void spawn_workers(Context& context) {
    //Context context = *new Context();
    for (int i = 0; i < NUM_WORKERS; ++i) {
        if (pthread_create(&context.threads[i], NULL, worker_thread, &context) != 0) {
            perror("pthread_create");
            exit(1);
        }
    }
}