#include "components.hpp"

uint16_t ComponentTable::entity_count = 0;

HeapAllocator* ComponentHeap = new HeapAllocator(COMPONENT_HEAP_SIZE);
ComponentTable component_table;

uint16_t ComponentTable::new_entity() {
    uint16_t result = entity_count;
    entity_count++;
    return result;
}