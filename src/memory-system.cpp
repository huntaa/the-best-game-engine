#include"memory-system.hpp"                                            
#include<iostream>

uint8_t alignment_padding(void *address, size_t alignment) {
    /*Returns amount of padding required to align *address* by *alignemnt* */
    uintptr_t rawAddress = (uintptr_t)address;
    size_t mask =(alignment - 1);
    uintptr_t misalignment = (rawAddress & mask);
    ptrdiff_t adjustment = alignment -misalignment;

    return adjustment;
}


HeapAllocator::HeapAllocator(size_t size_bytes) {
    _size = size_bytes;
    void* memory = malloc(size_bytes);
    head = (FreeBlock*)memory;
    head->block_size = size_bytes;
    head->next = NULL;
    num_mem_ptrs = 0;
    mem_ptrs[num_mem_ptrs] = memory;
    nBlocks = 1;
    
    //printf("%p\n", _memory_start);
}


void* HeapAllocator::allocate(size_t size_bytes, size_t alignment) {
    MetaData *metadata; //information corresponding to an allocated block of memory
    FreeBlock *index = head;
    FreeBlock *last = index;
    uint8_t *allocated = NULL;


//search for first block with sufficient size
    while((size_bytes + sizeof(MetaData) + alignment) > index->block_size){
        if(index->next != NULL) {
            last = index;
            index = index->next;
        } else {
            break;
        }
    }


//create a new free block that is doudble the size of the memory allocator until there is a freeblock large enough
    if((size_bytes + alignment + sizeof(MetaData)) > index->block_size){
        printf("not enough memory.... exiting\n");
        exit(1);
    }
    /*
    while((size_bytes + alignment + sizeof(MetaData)) > index->block_size) {
        num_mem_ptrs += 1;
        mem_ptrs[num_mem_ptrs] = malloc(_size*2);
        printf("malloc size: %d\n", _size*2);
        index->next = (FreeBlock*)mem_ptrs[num_mem_ptrs];
        last = index;
        index = index->next;
        index->next = NULL;
        index->block_size = _size * 2;
        _size *= 3;
        nBlocks += 1;
    }
    */

//initializing memory
    allocated = (uint8_t*)index;
    metadata = (MetaData*)index;
    size_t block_size = index->block_size;
    FreeBlock* next = index->next;
    metadata->data = size_bytes;
    void *result = (void*)(
        allocated + sizeof(MetaData)
    );
    uint8_t padding = alignment_padding(result, alignment);
    result = (void*)((uint8_t*)result + padding);
    *((uint8_t*)result - 1) = padding;  //byte address to contain amount address was adjusted by to meet byte alignment
    //head = (FreeBlock*)((uint8_t*)index + padding + sizeof(MetaData) + size_bytes)

    int total_allocated = sizeof(MetaData) + padding + size_bytes;
    index = (FreeBlock*)((uint8_t*)index + total_allocated);
    index->next = next;
    index->block_size = block_size - total_allocated;
    if(allocated == (uint8_t*)head) { //head needs to the end of the allocated block
        head = index;
    } else {
        last->next = index;
    }
    return result;
}


void HeapAllocator::merge() {
    FreeMemPtr index;
    bool merged;
    do {
        index.freeblock = head;
        merged = false;
        while(index.freeblock->next != NULL){
            if ((uint8_t*)index.freeblock->next == (index.rawaddr + index.freeblock->block_size)) {
                index.freeblock->block_size += index.freeblock->next->block_size;
                index.freeblock->next = index.freeblock->next->next;
                merged = true;
                break;
            } else {
                index.freeblock = index.freeblock->next;
            }
        }
    } while (merged);
}


void HeapAllocator::deallocate(void* memory) {
    FreeMemPtr deallocate;
    deallocate.rawaddr = (uint8_t*)memory;

    uint8_t padding = *(deallocate.rawaddr - 1);
    MetaData* meta = (MetaData*)(deallocate.rawaddr - padding - sizeof(MetaData));
    deallocate.rawaddr = (uint8_t*)meta;

    //try find adjacent free blocks and merge
    FreeMemPtr index;
    index.freeblock = head;

    while (deallocate.rawaddr > (index.rawaddr + index.freeblock->block_size)) {
        if (index.freeblock->next == NULL) {
            break;
        }
        index.freeblock = index.freeblock->next;
    }

    //create a new free block. This will always be at an address before the head
    nBlocks += 1;
    FreeMemPtr free;
    free.freeblock = deallocate.freeblock;
    free.freeblock->block_size = meta->data + sizeof(MetaData) + padding;
    if(free.rawaddr < (uint8_t*)head) {
        free.freeblock->next = head;
        head = free.freeblock;
    } else {
        free.freeblock->next = index.freeblock->next;
        index.freeblock->next  = free.freeblock;
    }
    merge();
}


void print_memory(HeapAllocator allocator) {
    HeapAllocator::FreeBlock* index = allocator.head;
    while(index != NULL) {
        printf("address: %p size: %lu, next: %p\n", index,  index->block_size, index->next);
        index = index->next;
    }
}


void zero_memory(void* memory, size_t length) {
    uint8_t* byte_memory = (uint8_t*)memory;
    for(uint8_t* index = byte_memory; index < byte_memory + length; index += 1) {
        *(index) = 0;
        //printf("%d\n", index.typeaddr[0]);
    }
}


void copy_memory(void* source, void* destination, size_t nbytes) {
    uint8_t* s_index = (uint8_t*) source;
    uint8_t* d_index = (uint8_t*) destination;
    for(size_t i = 0; i < nbytes; i++) {
        d_index[i] = s_index[i];
    }
}