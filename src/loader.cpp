#include"loader.hpp"
#include<iostream>
#include<iomanip>
#include<regex>
#include<string>
#include<stdlib.h>

void load_obj_file(const char* fileName, ModelData &model_data, glm::vec3 pos_offset) {
    // reads vertices out of model file and loads vertex/uv data and indeces into model_data.
    // returns the largest index value added to model_data.indices
    std::ifstream levelFile;
    levelFile.open(fileName);
    if(!levelFile.is_open()){
        std::cerr<<"error opening obj file: "<<strerror(errno)<<std::endl;
        exit(0);
    }
    std::string lineBuf;
    std::smatch sm;
    std::vector <glm::vec3> temp_vertices;
    std::vector <glm::vec2> temp_uvs;
    std::vector <glm::vec3> temp_normals;
    std::map<std::string, unsigned int> memoiz_dict;
    size_t index_counter = model_data.element_index_offset;   //index into model.data
    while(std::getline(levelFile, lineBuf)){
        //() is a capturing group, (?:) is a group but not a capturing group
        //? means optional
        if(std::regex_match(lineBuf, sm, std::regex("v (-?\\d+(?:\\.\\d+)?) (-?\\d+(?:\\.\\d+)?) (-?\\d+(?:\\.\\d+)?)"))){
            glm::vec3 vertex{std::stof(sm[1]), std::stof(sm[2]), std::stof(sm[3])};
            temp_vertices.push_back(vertex+pos_offset);
        } else if(std::regex_match(lineBuf, sm, std::regex("vt (-?\\d+(?:\\.\\d+)?) (-?\\d+(?:\\.\\d+)?)"))) {
            glm::vec2 vertex{std::stof(sm[1]), std::stof(sm[2])};
            temp_uvs.push_back(vertex);
        } else if(std::regex_match(lineBuf, sm, std::regex("f (\\d+)/(\\d+)/(\\d+) (\\d+)/(\\d+)/(\\d+) (\\d+)/(\\d+)/(\\d+)"))) {
            //copying vertices in the order declared in obj file, so that glDrawElemets can use it

            int vs[3]{std::stoi(sm[1])-1, std::stoi(sm[4])-1, std::stoi(sm[7])-1};
            std::vector <glm::vec3> verts{temp_vertices[vs[0]], temp_vertices[vs[1]], temp_vertices[vs[2]]};
            int uv_is[3]{std::stoi(sm[2])-1, std::stoi(sm[5])-1, std::stoi(sm[8])-1};
            std::vector <glm::vec2> uvs{temp_uvs[uv_is[0]], temp_uvs[uv_is[1]], temp_uvs[uv_is[2]]};

            //keys are the concatination of vertex index + uv index + normal index
            std::string key1 = std::string(sm[1]) + std::string(sm[2]) + std::string(sm[3]);
            if(memoiz_dict.count(key1) == 0){
                memoiz_dict[key1] = index_counter++;
                model_data.data.push_back(V_data{verts[0].x, verts[0].y, verts[0].z, uvs[0].x, uvs[0].y});
            }
            model_data.indices.push_back(memoiz_dict[key1]);

            std::string key2 = std::string(sm[4]) + std::string(sm[5]) + std::string(sm[6]);
            if(memoiz_dict.count(key2) == 0){
                memoiz_dict[key2] = index_counter++;
                model_data.data.push_back(V_data{verts[1].x, verts[1].y, verts[1].z, uvs[1].x, uvs[1].y});
            }
            model_data.indices.push_back(memoiz_dict[key2]);

            std::string key3 = std::string(sm[7]) + std::string(sm[8]) + std::string(sm[9]);
            if(memoiz_dict.count(key3) == 0){
                memoiz_dict[key3] = index_counter++;
                model_data.data.push_back(V_data{verts[2].x, verts[2].y, verts[2].z, uvs[2].x, uvs[2].y});
            }
            model_data.indices.push_back(memoiz_dict[key3]);
            //out_uvs.insert(out_uvs.end(), uvs.begin(), uvs.end());
            //face format: vertex/texture/normal which are all indices
        }
    }

    levelFile.close();
    model_data.element_index_offset += index_counter;
}
/*
int main(){
    Model model = load_obj_file("./assets/untitled.obj");
    return 0;
}
*/