#include <iostream>
#include "level.hpp"
#include "components.hpp"
#include <stdio.h>

void load_level(const char* filename, uint16_t *entities, std::vector<size_t>& model_ids){
    //reads level file and stores entities in 'entities' vector
    FILE* file = fopen(filename, "rb");
    component_type type;
    uint16_t entity;
    unsigned int entity_count = 0;
    while(fread(&type,sizeof(component_type), 1, file) > 0){
        if(type == component_type::new_entity) {
            unsigned int zero;
            fread(&zero, sizeof(unsigned int), 1, file);
            entity = component_table.new_entity();
            entities[entity_count++] = entity;
        }

        else if(type == component_type::position) {
            PositionComp pos;
            fread(&pos, sizeof(PositionComp), 1, file);
            pos.entity_id = entity;                     //each component says which entity it belongs to
            component_table.put<PositionComp>(pos, entity);
        }

        else if(type == component_type::model) {
            ModelComp model;
            fread(&model, sizeof(ModelComp), 1, file);
            model.entity_id = entity;
            model_ids.push_back(model.id);
            component_table.put<ModelComp>(model, entity);
        }

        else {
            std::cout<<"failed to read file format"<<std::endl;
            exit(0);
        }
    }

    fclose(file);
}

void get_models_of_type(unsigned int model_type, std::vector<ModelComp>& result){
    //collects all ModelComp components from the component table, and stores those with the specified model_type in result.
    std::cout<<"get models of type called"<<std::endl;
    ComponentVector<ModelComp>& models = component_table.get_all<ModelComp>();
    size_t i = 0;
    ModelComp model_comp = models[i];
    while(model_comp.entity_id != ENTITY_MAX) {
        if(model_comp.model_type == model_type) {
            result.push_back(model_comp);
        }
        i++;
        model_comp = models[i];
    }
}

void load_model_data(std::vector<ModelComp>& models, ModelData& result) {
    //model data loaded from those files specified in models, and appends them to result
    int i=0;

    for(auto& model: models) {
        size_t old_v_data_size = result.data.size(); //vertex data
        size_t old_i_data_size = result.indices.size(); //vertex indices data

        float factor = (float) model.entity_id;
        
        result.model_index_offsets.push_back(result.indices.size()); //offset of the next entry to be appended to result
        result.model_vertex_offsets.push_back(result.data.size());
        PositionComp pos_comp = component_table.get_one<PositionComp>(model.entity_id);

        load_obj_file(model.model_file, result, glm::vec3{pos_comp.x, pos_comp.y , pos_comp.z});
        if(result.model_vertex_sizes.capacity() <= i) {
            result.model_vertex_sizes.resize(result.model_vertex_sizes.size() + 10);
            result.model_index_sizes.resize(result.model_index_sizes.size() + 10);
        }
        result.model_vertex_sizes[i] = result.data.size() - old_v_data_size;        //store size of model loaded
        result.model_index_sizes[i] = result.indices.size() - old_i_data_size;
        i++;
    }
}