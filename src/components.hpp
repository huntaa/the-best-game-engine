#pragma once
#include "memory-system.hpp"
#include <stdlib.h>
#include <limits>

const auto ENTITY_MAX = std::numeric_limits<uint16_t>::max();

#define VEC_LEN 100 //number of entities
#define COMPONENT_HEAP_SIZE 819200
#define MAX_ENTITY_COUNT 1000


extern HeapAllocator* ComponentHeap;

//singleton class


template<class T>
class ComponentVector {
        Vec<T>* data;
        //std::vector<T>* data;
        union t_ptr {
                T* data;
                uint16_t* entity_id;
        };
        ComponentVector() { //cant create instance of class from outside the class' scope because constructor is private
                //data = new T[100];
                index_table = new IndexTable();
                indexed = false;
                uint16_t *ptr;
                uint16_t *other_ptr;

                data = new Vec<T>(ComponentHeap, VEC_LEN);

                for(int i=0; i<VEC_LEN; i++) {
                        ptr = (uint16_t*)&(*data)[i];
                        *ptr=ENTITY_MAX;
                }
        }

        class IndexTable {
                uint16_t index[VEC_LEN];
                uint16_t n_entries;
        public:
                IndexTable(){
                        n_entries = 0;
                }
                uint16_t add_index(uint16_t source_index) {
                        index[source_index] = n_entries;
                        n_entries= n_entries + 1;
                        return index[source_index];
                }
                uint16_t operator[](size_t idx) {
                        if(idx > n_entries) {
                                printf("Component vector error: cannot retrieve item because it does not exist\n");
                        }
                        return index[idx];
                }
        };

public:
        IndexTable *index_table; //this should be stored in componentHeap to ensure nice cache locality
        bool indexed;
        T& operator[](uint16_t idx) { //& in this case just means that we want to return the original object and not make a copy of it
                //data->print();
                if(indexed) {
                        idx = (*index_table)[idx];
                }
                return (*data)[idx];
        }

        static ComponentVector<T>& get() {
                static ComponentVector<T> instance;
                return instance;
        }

        void insert(T component, uint16_t position) {
                if(indexed) {
                        (*index_table).add_index(position);
                        position = (*index_table)[position];
                }
                if(data->insert(component, position) == false){
                        printf("not enough space in vector to insert this component... exiting\n");
                        exit(0);
                }
        }
};

class ComponentTable {
        static uint16_t entity_count;
        static ComponentTable* self;
public:
        /*static ComponentTable& init() {
                ComponentHeap = new HeapAllocator(COMPONENT_HEAP_SIZE);
                self = new ComponentTable(*ComponentHeap);
                return *self;
        }*/


        static uint16_t new_entity();

        template <class T>
        static void indexed() {
                // turns on indexing for component vector of given component type
                ComponentVector<T>::get().indexed = true;
        }

        template<class T>
        T& get_one(int ent_id) {
                return ComponentVector<T>::get()[ent_id];
        }

        template<class T>
        ComponentVector<T>& get_all(){
                return ComponentVector<T>::get();
        }

        template <class T>
        void put(T component, uint16_t entity) {
                ComponentVector<T> compvec = ComponentVector<T>::get();
                compvec.insert(component, entity);
        }

};

extern ComponentTable component_table;

struct PositionComp {
        uint16_t entity_id;
        float x;
        float y;
        float z;
};

struct ModelComp{
        uint16_t entity_id;
        size_t id;
        size_t model_type;
        const char* texture;
        const char* model_file;
};