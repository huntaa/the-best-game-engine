/*
    Each system should have its own main memory allocator so that they can run in parallel without
    having to worry about accessing the same parts in memory.
 */

#pragma once

#include<stdlib.h>
#include<iostream>


void zero_memory(void* memory, size_t length);

void copy_memory(void* source, void* destination, size_t nbytes);


class HeapAllocator{
private:
    void merge();
public:
    void* mem_ptrs[128];    //pointers to blocks of memory allocaed by calling malloc
    size_t num_mem_ptrs;
    struct MetaData {
        size_t data;       //<- size of the reserved memory
    };
    struct FreeBlock {
        FreeBlock *next;
        size_t block_size;
    };
    union FreeMemPtr {
        FreeBlock* freeblock;
        uint8_t* rawaddr;
    };
    size_t nBlocks;
    FreeBlock *head;
    size_t _size;
    void* allocate(size_t size_bytes, size_t alignment=8);
    void deallocate(void* memory); 
    HeapAllocator(size_t size);
};


void print_memory(HeapAllocator allocator);


template <class T>
class Vec {
    HeapAllocator* parent_allocator;

    union VecMember {
        T* typeaddr;
        uint8_t* rawaddr;
    };

    VecMember memory_start;
    VecMember head;                     //<- next free space

    void reallocate();

public:
    size_t vec_len;
    Vec(HeapAllocator* parent, size_t length);
    void append(T item);
    T& get(int item_index);
    bool insert(T component,int item_index);
    T& operator[](uint16_t position);
    void print();
};




template <class T>
Vec<T>::Vec(HeapAllocator* parent, size_t length) {
    parent_allocator = parent;
    vec_len = length;
    memory_start.rawaddr = (uint8_t*)parent_allocator->allocate(vec_len*sizeof(T), sizeof(T));
    printf("memory start: %p\n", memory_start.typeaddr);
    head = memory_start;
    for(VecMember index = memory_start; index.typeaddr < memory_start.typeaddr + vec_len; index.typeaddr += 1) {
        *(index.rawaddr) = 0;
    }
}


template <class T>
void Vec<T>::reallocate() {
    size_t new_vec_len = vec_len*2;
    VecMember newaddr;
    newaddr.rawaddr = (uint8_t*)parent_allocator->allocate(new_vec_len*sizeof(T), sizeof(T));
    printf("new memory start: %p\n", newaddr.typeaddr);
    zero_memory((void*)newaddr.rawaddr, new_vec_len);
    copy_memory((void*)memory_start.rawaddr, (void*)newaddr.rawaddr, new_vec_len * sizeof(T));
    parent_allocator->deallocate((void*)memory_start.rawaddr);
    
    memory_start = newaddr;
    head.typeaddr = memory_start.typeaddr + vec_len;
    vec_len = new_vec_len;
}


template <class T>
void Vec<T>::append(T item){
    while(head.typeaddr >= (memory_start.typeaddr + vec_len)) {
        printf("doubling size\n");
        reallocate();
    }
    head.typeaddr[0] = item;
    head.typeaddr += 1;
}


template <class T>
bool Vec<T>::insert(T component, int item_index){
    if ((memory_start.typeaddr + item_index) >= (memory_start.typeaddr + vec_len)) {
        //printf("vector ran out of memory... exiting\n");
        //reallocate();
        return false;
    }
    memory_start.typeaddr[item_index] = component;
    if(memory_start.typeaddr + item_index > head.typeaddr) {
        head.typeaddr = memory_start.typeaddr + item_index;
    }
    return true;
}

#include <iostream>

template <class T>
T& Vec<T>::get(int item_index) {
    VecMember index = memory_start;
    /*if((index.typeaddr + item_index) > head.typeaddr) {
        printf("the item was not found\n");
    }*/

    return index.typeaddr[item_index];

}

template <class T>
T& Vec<T>::operator[](uint16_t position){

    return get(position);
}

template <class T>
void Vec<T>::print() {
        for(VecMember index = memory_start; index.typeaddr < head.typeaddr; index.typeaddr += 1) {
        printf("index address: %p value: %d\n", index.typeaddr, index.typeaddr[0]);
    }
}