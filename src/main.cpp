#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <chrono>
#include "components.hpp"
#include "memory-system.hpp"
#include "task-queue.hpp"
#include "loader.hpp"
#include "renderer.hpp"
#include "level.hpp"
 
const int num_entities = VEC_LEN;
uint16_t entities[num_entities];


//TODO: Vec<T>::insert() needs to check if it has enough space before inserting. DONE
//TODO: Vec<T>::insert() should not just double size when it has no room... could break ECS


int main() {
    const char * filename = "levelfile";
    FILE* file = fopen(filename, "wb");
    write_component<unsigned int>(file, component_type::new_entity, 0);
    write_component<PositionComp>(file, component_type::position, PositionComp{ENTITY_MAX,1.5f, 2.5f, 1.5f});
    write_component<ModelComp>(file, component_type::model, ModelComp{ENTITY_MAX, 1, DYNAMIC_MODEL_DATA, "./assets/face.png", "./assets/untitled.obj"});

    write_component<unsigned int>(file, component_type::new_entity, 0);
    write_component<PositionComp>(file, component_type::position, PositionComp{ENTITY_MAX, 0.5f, 0.5f, 0.5f});
    write_component<ModelComp>(file, component_type::model, ModelComp{ENTITY_MAX, 2, DYNAMIC_MODEL_DATA, "./assets/face.png", "./assets/untitled.obj"});

    write_component<unsigned int>(file, component_type::new_entity, 0);
    write_component<PositionComp>(file, component_type::position, PositionComp{ENTITY_MAX, -1.0f, 1.0f, -1.0f});
    write_component<ModelComp>(file, component_type::model, ModelComp{ENTITY_MAX, 3, LEVEL_MODEL_DATA, "./assets/boxtexture.png", "./assets/box.obj"});

    fclose(file);
    std::vector<size_t> model_ids;
    load_level(filename, entities, model_ids);

    ModelData model_data1;
    ModelData model_data2;
    /*iterate over all model components and load all models of type LEVEL_MODEL_DATA into a 
    contiguous buffer with correct indexes*/

    std::vector<ModelComp> level_models;
    get_models_of_type(LEVEL_MODEL_DATA, level_models);
    load_model_data(level_models, model_data1);

    std::vector<ModelComp> dynamic_models;
    get_models_of_type(DYNAMIC_MODEL_DATA, dynamic_models);
    load_model_data(dynamic_models, model_data2);
    //BatchTable batch_table(&model_ids);
    Renderer renderer(&model_ids);
    renderer.load(level_models, model_data1, LEVEL_MODEL_DATA ,level_models[0].texture);
    renderer.load(dynamic_models, model_data2, DYNAMIC_MODEL_DATA, dynamic_models[0].texture);
    print_memory(*ComponentHeap);
    /*
    Context context = *new Context(NUM_WORKERS, QUEUE_SIZE, &component_table);
    spawn_workers(context);

    for(int i=0; i<num_entities; i++) {
        entities[i] = iiiiicomponent_table.new_entity();
        component_table.put<ChunkComp>(ChunkComp{(uint64_t)i, true}, entities[i]);
        context.todo->push(Task{INT, entities[i]});
        component_table.put<long>((long)i, entities[i]);
        context.todo->push(Task{LONG, entities[i]});
    }
    std::cout<<"id value:" << component_table.get_comp<ChunkComp>(2).id <<std::endl;
    std::cout<<"vec len: " << VEC_LEN << "size of chunk comp: " << sizeof(ChunkComp) << "size of long: " << sizeof(long)<< std::endl;
    print_memory(*ComponentHeap);
    */

    //loader loader(&component_table);2
    //printf("got here\n");
    //loader.load_chunk(0);

 
    /*ComponentTable::init();
    print_memory(*ComponentHeap);
 
    Context context = *new Context(NUM_WORKERS, QUEUE_SIZE);
    spawn_workers(context);
 
    for(int i=0; i<num_entities; i++) {
        entities[i] = ComponentTable::new_entity();
        ComponentTable::put<int>(i, entities[i]);
        context.todo->push(Task{INT, entities[i]});
        ComponentTable::put<long>((long)i, entities[i]);
        context.todo->push(Task{LONG, entities[i]});
 
    }
    */
    //print_memory(*ComponentHeap);
    // ComponentTable::init();
    // print_memory(*ComponentHeap);
    // uint16_t entity1 = ComponentTable::new_entity();
    // ComponentTable::put<int>(5, entity1);
    // uint16_t entity2 = ComponentTable::new_entity();
    // ComponentTable::put<int>(7, entity2);
    // print_memory(*ComponentHeap);
    // ComponentTable::indexed<long>();
    // ComponentTable::put<long>(9, entity1);
    // ComponentTable::put<long>(18, entity2);
    // /*ComponentTable::indexed<long>();
    // ComponentTable::put<long>(5);
    // ComponentTable::put<long>(7);
    // ComponentTable::put<long>(9);
    // print_memory(*ComponentHeap);*/
    // printf("int component value: %lu\n", ComponentTable::get_comp<int>(entity1));
    // printf("long component value: %lu\n", ComponentTable::get_comp<long>(entity1));
    // //printf("sizeof component vector: %lu\n", sizeof(ComponentVector<long>));
    // //printf("long component value: %d\n", ComponentTable::get_comp<long>(2));
    // for(int i=0; i<QUEUE_SIZE; i++) {
    //     context.todo->push(Task{i});
    // }
    while (!glfwWindowShouldClose(renderer.window))
    {
        //auto t_start = std::chrono::high_resolution_clock::now();
        //glUniformMatrix4fv(renderer.uniTrans, 1, GL_FALSE, glm::value_ptr(renderer.trans));
        //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        //glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, 0);
        //glDrawElements(GL_TRIANGLES, model.indices.size(), GL_UNSIGNED_INT, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        renderer.render();


        //auto t_now = std::chrono::high_resolution_clock::now();
        //float time = std::chrono::duration_cast<std::chrono::duration<float>>(t_now - t_start).count();
        //renderer.trans = glm::rotate(renderer.trans, 0.5f * time * glm::radians(180.0f),glm::vec3(0.0f, 0.0f, 1.0f));
        glfwSwapBuffers(renderer.window);
    }

    glfwDestroyWindow(renderer.window);
    glfwTerminate();
    return 0;
}
