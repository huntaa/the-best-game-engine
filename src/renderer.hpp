#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <vector>
#include <map>
#include "loader.hpp"
#include "components.hpp"

// chunks defined in level file


#define MIN_BATCH_SIZE 1000
#define MAX_BATCH_SIZE 10000
/*sizes of maximum and minumum gl buffer size that corresponds to each batch*/
class Batch {
    //currently one batch for each "model type"
    GLuint VAOs[1];
    GLuint VBO;
    GLuint EBO;
    GLint posAttrib;
    GLint texAttrib;
    glm::mat4 proj;
    GLint uniProj;
    size_t v_head_offset;
    size_t i_head_offset;
    size_t buffer_sizes;
    GLint uniView;
    glm::mat4 view;
    glm::mat4 trans;
    GLint uniTrans;
    //maps model_id to location in model_data buffer as an offset in bytes
    //{model_id: {byte offset, size_bytes}}
    std::map<size_t, std::pair<size_t, size_t>> model_vertex_offsets;
    std::map<size_t, std::pair<size_t, size_t>> model_index_offsets;
    std::vector<char>* texture_atlas_data;
    GLuint texture_id;
    int texture_unit; 
    //loads model data into opengl buffer at given offset
    void _load_model_to(ModelData& model_data, size_t v_offset_bytes, size_t v_size_bytes, size_t i_offset_bytes, size_t i_size_bytes);

public:
    size_t element_index_offset;
    GLuint vertexShader;
    GLuint fragmentShader;
    GLuint shaderProgram;
    Batch(size_t size_bytes, int texture_unit);
    bool has_capacity(size_t vertices_size_bytes, size_t indices_size_bytes);
    //load model data into opengl buffer
    void load(std::vector<ModelComp>& models_info, ModelData& model_data, const char* texture_filename);
    // unload model data from gl buffer
    void render(); //called by Renderer
};


class BatchTable {
    friend class Renderer;
    //model_ids declared in level file, and entries are added for all model_ids when level file is loaded
    //{model_id: batch_index}
    std::map<size_t, size_t> batch_table; //maps model_id to batch index
    std::vector<Batch> batches;
public:
    size_t batch_count;
    BatchTable(std::vector<size_t>* model_ids);
    void load(std::vector<ModelComp>& models_info, ModelData& model_data, size_t batch_id, const char* texture_filename);
    bool is_loaded(size_t model_id);
    size_t create_batch(size_t batch_size_bytes, int texture_unit);
};

class Renderer {
    //level file must be loaded before renderer can be intialized
    //GLuint VAOs[1];
    BatchTable batch_table;
    size_t static_batch;
    size_t dynamic_batch;
    size_t level_batch;
    glm::mat4 trans;
    GLint uniTrans;
public:
    Renderer(std::vector<size_t>* model_ids);
    GLFWwindow* window;
    void render();

    void load(std::vector<ModelComp>& models_info, ModelData& model_data, size_t model_type, const char *texture_filename); //temporary function... should be done by the task queue/ worker threads
    //bool load_current_buffer();
};