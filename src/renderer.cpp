#include "renderer.hpp"

void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

Renderer::Renderer(std::vector<size_t>* model_ids): batch_table(model_ids){
    glfwSetErrorCallback(error_callback);
    if(!glfwInit()) {
        printf("error initializing glfw");
        exit(-1);
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    window = glfwCreateWindow(1920, 1080, "the-best-game-engine", NULL, NULL);

    if(!window) {
        printf("window failed to initialize");
        exit(-1);
    }

    glfwMakeContextCurrent(window);
    GLenum err = glewInit();

    if (GLEW_OK != err)
    {
    /* Problem: glewInit failed, something is seriously wrong. */
    fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
    }
    fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
 
 
    if (!GLEW_VERSION_4_1)
    {
        printf("opengl version 4.1 is not supported\n");
    }

    glActiveTexture(GL_TEXTURE0);
    dynamic_batch = batch_table.create_batch(100000, 0);
    glActiveTexture(GL_TEXTURE1);
    level_batch = batch_table.create_batch(100000, 1);
    static_batch = 2;
}


Batch::Batch(size_t size_bytes, int texture_unit) {
    glGenVertexArrays(1, VAOs);
    glBindVertexArray(VAOs[0]);
    const char * vertexSource = R"glsl(
       #version 150 core
 
       in vec3 position;
       in vec2 texcoord;
 
       uniform mat4 model;
       uniform mat4 view;
       uniform mat4 proj;
 
       out vec2 Texcoord;
 
       void main() {
           Texcoord = texcoord;
           gl_Position = proj * view * model * vec4(position, 1.0);
       }
    )glsl";
 
    //outputs from the vertex shader are given to the graphics driver, and interpolations of these values are given as inputs to the fragment shader
    const char * fragmentSource = R"glsl(
       #version 150 core
       in vec2 Texcoord;
 
       uniform sampler2D texFace;
 
       out vec4 outColor;
 
       void main() {
           vec4 texColor = texture(texFace, Texcoord);
           outColor = texColor;
       }
    )glsl";

    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertexShader, 1, &vertexSource, NULL); //<- stop at NULL terminator
    glCompileShader(vertexShader);

    GLint status;
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
    if(status == GL_TRUE) {
        printf("vertex shader compiled successfully\n");
    }

    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
    glCompileShader(fragmentShader);

    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
    if(status == GL_TRUE) {
        printf("fragment shader compiled successfully\n");
    }

    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);

    glBindFragDataLocation(shaderProgram, 0, "outColor");
    
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);
    glEnable(GL_DEPTH_TEST);  

    glGenTextures(1, &texture_id);
    glBindTexture(GL_TEXTURE_2D, texture_id);
    this->texture_unit = texture_unit;
    glUniform1i(glGetUniformLocation(shaderProgram, "texFace"), texture_unit);

    //glBindBuffer(GL_ARRAY_BUFFER, VBO);
    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    glEnableVertexAttribArray(posAttrib);

    GLint texAttrib = glGetAttribLocation(shaderProgram, "texcoord");
    glEnableVertexAttribArray(texAttrib);
    view = glm::lookAt(
        glm::vec3(1.2f, 0.2f, 8.2f),
        glm::vec3(0.0f, 2.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 1.0f)
    );
    uniView = glGetUniformLocation(shaderProgram, "view");
    glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(view));
 
    proj = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 1.0f, 10.0f);
    uniProj = glGetUniformLocation(shaderProgram, "proj");
    glUniformMatrix4fv(uniProj, 1, GL_FALSE, glm::value_ptr(proj));

    trans = glm::mat4(1.0f);
    trans = glm::rotate(trans, glm::radians(130.0f), glm::vec3(0.0f, 1.0f, 1.0f));
    uniTrans = glGetUniformLocation(shaderProgram, "model");
    glUniformMatrix4fv(uniTrans, 1, GL_FALSE, glm::value_ptr(trans));

    this->posAttrib = posAttrib;
    this->texAttrib = texAttrib;
    buffer_sizes = size_bytes;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(posAttrib, 3, GL_FLOAT, GL_FALSE, 5*sizeof(float), 0);
    glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(3*sizeof(float)));
    glBufferData(GL_ARRAY_BUFFER, size_bytes, 0, GL_DYNAMIC_DRAW);
    v_head_offset = 0;

    glGenBuffers(1, &EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, size_bytes, 0, GL_DYNAMIC_DRAW);
    i_head_offset = 0;
    element_index_offset = 0;
}

void Batch::load(std::vector<ModelComp>& models_info, ModelData& model_data, const char* texture_filename) {
    //assumes BatchTable.load() function has already determined there is sufficient space to contain model data
    glBindVertexArray(VAOs[0]);
    glUseProgram(shaderProgram);
    glBindTexture(GL_TEXTURE_2D, this->texture_id);
    size_t i = 0;
    for(auto model : models_info){
        model_vertex_offsets.insert({model.id, std::pair<size_t, size_t>{model_data.model_vertex_offsets[i], model_data.model_vertex_sizes[i]*sizeof(V_data)}});
        model_index_offsets.insert({model.id, std::pair<size_t, size_t>{model_data.model_index_offsets[i], model_data.model_index_sizes[i]*sizeof(unsigned int)}});
        i++;
    }
    size_t total_v_data_size = model_data.data.size()*sizeof(V_data);
    size_t total_i_data_size = model_data.indices.size()*sizeof(unsigned int);
    _load_model_to(
        model_data,
        v_head_offset,
        total_v_data_size,
        i_head_offset,
        total_i_data_size
    );
    v_head_offset += total_v_data_size;
    i_head_offset += total_i_data_size;
    //element_index_offset += model_data.indices.size();

    unsigned char* image;
    int width, height;
    image = SOIL_load_image(texture_filename, &width, &height, 0, SOIL_LOAD_RGB);
    float color[] = { 1.0f, 0.0f, 0.0f, 1.0f };
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, color);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
    SOIL_free_image_data(image);
}

void Batch::_load_model_to(ModelData& model_data, size_t v_offset_bytes, size_t v_size_bytes, size_t i_offset_bytes, size_t i_size_bytes) {
    //loads model data into opengl buffer at given offset
    std::cout<<"model data "<<*model_data.data[6].vertex<<std::endl;
    std::cout<<"size: "<<sizeof(unsigned int)*(model_data.indices.size())<<std::endl;
    std::cout<<"offset "<<i_offset_bytes<<std::endl;
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferSubData(GL_ARRAY_BUFFER, v_offset_bytes, v_size_bytes, model_data.data.data());
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, i_offset_bytes, i_size_bytes, model_data.indices.data());
}

bool Batch::has_capacity(size_t vertices_size_bytes, size_t indices_size_bytes) {
    return (vertices_size_bytes < buffer_sizes-v_head_offset) 
        and (indices_size_bytes < buffer_sizes-indices_size_bytes);
}

void Batch::render() {
    /*
    std::cout<<"VBO: "<<(unsigned int) VBO<<std::endl;
    std::cout<<"EBO: "<<(unsigned int) EBO<<std::endl;
    std::cout<<"shader program: "<<(unsigned int) shaderProgram<<std::endl;
    std::cout<<"texture id: "<<(unsigned int) texture_id<<std::endl;
    std::cout<<"texture_unit: "<<(int) texture_unit<<std::endl;
    */

    glBindVertexArray(VAOs[0]);
    glBindTexture(GL_TEXTURE_2D, texture_id);
    glUseProgram(shaderProgram);
    /*
    glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(view));
    //glBindTexture(GL_TEXTURE_2D, texture_id);
    glUniform1i(glGetUniformLocation(shaderProgram, "texFace"), texture_unit);
    glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(uniProj, 1, GL_FALSE, glm::value_ptr(proj));
    glUniformMatrix4fv(uniTrans, 1, GL_FALSE, glm::value_ptr(trans));
    */
    //glUniformMatrix4fv(uniView, 1, GL_FALSE, glm::value_ptr(view));
    //glUniformMatrix4fv(uniProj, 1, GL_FALSE, glm::value_ptr(proj));
    //glUniformMatrix4fv(uniTrans, 1, GL_FALSE, glm::value_ptr(trans));
    //glUniform1i(glGetUniformLocation(shaderProgram, "texFace"), 0);
    //glDrawElements(); called once to render all non-instanced objects in batch
    //glDrawElementsInstanced(); for each instanced object in batch
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glDrawElements(GL_TRIANGLES, i_head_offset / sizeof(unsigned int), GL_UNSIGNED_INT, 0);
}

#define NO_BATCH_BOUND -1

BatchTable::BatchTable(std::vector<size_t>* model_ids){
    //called after level file has been loaded
    batch_count=0;
    for(auto i=model_ids->begin(); i<model_ids->end(); i++) {
        //initialize all values to -1
        batch_table.insert({*i, NO_BATCH_BOUND});
    }

}

bool BatchTable::is_loaded(size_t model_id) {
    return batch_table[model_id] != NO_BATCH_BOUND;
}

size_t BatchTable::create_batch(size_t batch_size_bytes, int texture_unit) {
    //creates batch of size batch_size_bytes and returns it's handle
    batches.push_back(Batch(batch_size_bytes, texture_unit));
    return batch_count++;
}

void BatchTable::load(std::vector<ModelComp>& models_info, ModelData& model_data, size_t batch_id, const char* texture_filename) {
    size_t vertices_data_size = model_data.data.size()*sizeof(V_data);
    size_t indices_data_size = model_data.indices.size()*sizeof(unsigned int);

    //batches[batch_id].element_index_offset += model_data.element_index_offset;
    if(batches[batch_id].has_capacity(vertices_data_size, indices_data_size)){
        batches[batch_id].load(models_info, model_data, texture_filename);
        //batch_table.insert({model_id, batch_id});
        for(auto model_info: models_info){
            batch_table[model_info.id] = batch_id;
        }
        return;
    }
    /*
    //try to put model into any batch that has free space
    size_t count = 0;
    for(auto i=batches.begin(); i<batches.end(); i++) {
        if(i->has_capacity(vertices_data_size, indices_data_size)){
            i->load(model_id, model_data);
            batch_table.insert({model_id, count});
            return;
        }
        count++;
    }
    */
    std::cout<<"no batch has sufficient space to load model into"<<std::endl;
    exit(1);
}

void Renderer::render() {
    //for(auto i=batch_table.batches.begin(); i<batch_table.batches.end(); i++) {
    //    i->render();
    //}
    batch_table.is_loaded(1);
    //glBindVertexArray(VAOs[0]);
    for(int i=0; i<batch_table.batch_count; i++){
        batch_table.batches[i].render();
    }
}

void Renderer::load(std::vector<ModelComp>& models_info, ModelData& model_data, size_t model_type, const char *texture_filename) {
    size_t batch_id;
    if(model_type == DYNAMIC_MODEL_DATA) {
        batch_id = dynamic_batch;
    } else if(model_type == LEVEL_MODEL_DATA) {
        batch_id = level_batch;
    } else {
        batch_id = static_batch;
    }
    batch_table.load(models_info, model_data, batch_id, texture_filename);
}