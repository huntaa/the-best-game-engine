#pragma once

#include<string.h>
#include<fstream>
#include<vector>
#include<map>
#include<glm/glm.hpp>
#include<GL/glew.h>

#define STATIC_MODEL_DATA 0
#define LEVEL_MODEL_DATA 1
#define DYNAMIC_MODEL_DATA 2

struct V_data {
    float vertex[5];
};


struct ModelData {
    ModelData(): element_index_offset(0){};
    std::vector <V_data> data;
    std::vector <unsigned int> indices;
    size_t element_index_offset;
    std::vector<size_t> model_index_offsets; //offsets of individual models in order they were loaded
    std::vector<size_t> model_vertex_offsets;
    std::vector<size_t> model_index_sizes;
    std::vector<size_t> model_vertex_sizes;
};
//called by Batch loader
//model_data created and managed by chunk loader
void load_obj_file(const char* filename, ModelData &model_data, glm::vec3 pos_offset);

struct ChunkPos{
    float x;
    float y;
};
class Chunk{
    ChunkPos pos_a;
    ChunkPos pos_b;
    //boundary of chunk defined by two points (rectangle) in world coordinates
    void load_from_file();
public:
    //chunks should keep track of which models they are 
    std::vector<int> entities;//should probably be unsigned
};