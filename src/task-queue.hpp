#include<boost/lockfree/queue.hpp>
#include<stdlib.h>
#include<pthread.h>
#include<semaphore.h>
//https://www.boost.org/doc/libs/1_72_0/doc/html/boost/lockfree/queue.html

#include"components.hpp"

//#define NUM_TASKS 1000
#define NUM_WORKERS 8
#define QUEUE_SIZE 100


enum system_type {INT, LONG};
struct Task {
    //system_type system;
    //void (*system)(void);       //<--pointer to system method
    system_type st;
    int number;                   //<-- entity id
    //int range[2];
};


typedef boost::lockfree::queue<Task> TaskQueue;


class TaskQueueWrapper {
    /*wrapper for taskfree queue*/
    sem_t tasks_full;
    sem_t tasks_empty;
    TaskQueue* task_queue;
public:
    TaskQueueWrapper(int queue_len);
    ~TaskQueueWrapper();
    void free();
    void push(Task task);
    bool pop(Task& ret);
};

/*
struct Context {
    QueueSystem* taskqueue;
    pthread_t* threads;
    int num_workers;

};*/


struct Context{
    Context(int num_workers, int queue_size, ComponentTable* component_table);
    TaskQueueWrapper *todo;
    ComponentTable* component_table;
    pthread_t *threads;
    int num_workers;

};

//Context* spawn_workers(int num_workers);
void spawn_workers(Context& context);

void* worker_thread(void* arg);