#pragma once
#include<vector>
#include<stdlib.h>
#include "components.hpp"
#include "loader.hpp"

enum component_type {
    new_entity,
    position,
    model
};

template<class T>
void write_component(FILE* file, component_type type, const T& value) {
    fwrite(&type, sizeof(component_type), 1, file);
    fwrite(&value, sizeof(T),1, file);
}
void load_level(const char* filename, uint16_t *entities, std::vector<size_t>& model_ids);

void get_models_of_type(unsigned int model_type, std::vector<ModelComp>& result);
void load_model_data(std::vector<ModelComp>& models, ModelData& result);