# the-best-game-engine
![alt text](https://img.maximummedia.ie/joe_co_uk/eyJkYXRhIjoie1widXJsXCI6XCJodHRwOlxcXC9cXFwvbWVkaWEtam9lY291ay5tYXhpbXVtbWVkaWEuaWUuczMuYW1hem9uYXdzLmNvbVxcXC93cC1jb250ZW50XFxcL3VwbG9hZHNcXFwvMjAxOVxcXC8wM1xcXC8wNDE2NTgwN1xcXC9TY3JlZW5zaG90LTIwMTktMDMtMDQtYXQtMTYuNTcuNDgucG5nXCIsXCJ3aWR0aFwiOjc2NyxcImhlaWdodFwiOjQzMSxcImRlZmF1bHRcIjpcImh0dHBzOlxcXC9cXFwvd3d3LmpvZS5jby51a1xcXC9hc3NldHNcXFwvaW1hZ2VzXFxcL2pvZWNvdWtcXFwvbm8taW1hZ2UucG5nP3Y9MjJcIixcIm9wdGlvbnNcIjpbXX0iLCJoYXNoIjoiNzc2YTU0Y2IwMDRlZGQ2ZTM5MTcwZDMxZWYzODI1ZTljNzFkNTQyOCJ9/screenshot-2019-03-04-at-16-57-48.png "inspiration")

## What Does it Do?
currently nothing useful...
* Initializes the custom memory allocator
* sets up the component table which is allocated memory from the memory allocator
* creates a few dummy entities and assigns them a couple of dummy components
* initializes the task queue, spawns some threads, pushes some dummy tasks to the queue...
* loads an .obj file and and stores it into an open gl buffer in an interleaved format.
* loads corresponding textures

* drawns spinning 3d model

# TODO:

## API:
* Precompiled headers
* game engine API as shared (dynamically linked) library containing main() with entrypoint being defined by the application linking the API
* logging

### Event System:
* Researching...

### levels:
* Binary space partitioning
* all entities and their components to be loaded into component table when level is loaded.
* level files:
   * define space partitions(chunks)
   * chunk component

### ECS:
* indexing entities by a set of components

### task system:
* tasks to specify a range of entities so that multiple threads can be working on the same task type

### loader:
* BSP used for space partitioning
* selects active chunks as player moves about world
* active chunks:
   * models in chunks are loaded into memory
   * other stuff can be happening in active chunks

### renderer:
* OpenGL:
   * version 4.1
   * glew 
   * glfw 
* 3D perspective
* Batch rendering
   * Model data is loaded into "batches" when chunks are loaded.
   * Each batch should keep indexes to each model stored in contiguous blocks
   * A batch corresponds to one or more chunks
   * Implementation 1: 
      * Gl buffers in batches managed by heap allocator container
      * Assets get unloaded when there are no references to them in active chunks
      * Few large batches
      * Expecting more blocking calls to graphics driver but less data sent to GPU
   * Implementation 2:
      * lots of small batches
      * batches can be appended to, but individual models are never unloaded
      * entire batches are to be unloaded at a time
      * when some amount of model data in a batch is not in use, the batch is unloaded, and the model data that is in use is loaded in to a different batch.
      * which models are loaded into which batches depending on state of active chunks could be optimized using simplex method on game compile stage(unsure), otherwise possibly optimized using machine learning
   * When a chunk is being loaded, try to add any new model data to an existing batch.
   * Texture atlas' are dynamically generated as model data is being loaded into them
* draw models:
   * textures
      * texture atlas'
   * vertices
   * maps
* buffers:
   * model/VBO table
   
